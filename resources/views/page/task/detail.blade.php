@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">Detail Task</div>
                <div class="card-body">
					<div class="form-group row">
						<label for="prodName" class="col-sm-2 col-form-label">Title <em>*</em></label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="title" value="{{ $data['title'] }}" id="title" readonly />
						</div>
					</div>
					<div class="form-group row">
						<label for="prodDesc" class="col-sm-2 col-form-label">Description </label>
						<div class="col-sm-10">
							<textarea class="form-control" name="description" row="5" id="description" readonly>{{ $data['description'] }}</textarea>
						</div>
					</div>
					<div class="form-group row">
						<label for="prodName" class="col-sm-2 col-form-label">Start Date</label>
						<div class="col-sm-10">
							<div class="row">
								<div class="col-md-6">
									<input type="text" class="form-control" name="start_date" value="{{ date('d-m-Y',strtotime($data['start_date'])) }}" readonly />
								</div>
								<div class="col-md-6">
									<input type="text" class="form-control" name="start_time" value="{{ date('H:i',strtotime($data['start_time'])) }}" readonly />
								</div>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label for="prodName" class="col-sm-2 col-form-label">End Date</label>
						<div class="col-sm-10">
							<div class="row">
								<div class="col-md-6">
									<input type="text" class="form-control" name="end_date" value="{{ (!empty($data['end_date'])) ? date('d-m-Y',strtotime($data['end_date'])) : null }}" readonly />
								</div>
								<div class="col-md-6">
									<input type="text" class="form-control" name="end_time" value="{{ (!empty($data['end_time'])) ? date('H:i',strtotime($data['end_time'])) : null }}" readonly />
								</div>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label for="prodDesc" class="col-sm-2 col-form-label">Status </label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="status" value="{{ $data['status'] }}" id="status" readonly />
						</div>
					</div>
					<div class="form-group row">
						<label for="prodDesc" class="col-sm-2 col-form-label">Assignee </label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="assignee" value="{{ $data['assignee_user']['name'] }}" id="status" readonly />
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 text-right">
							<a href="/" class="btn btn-md btn-link">Back</a>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection