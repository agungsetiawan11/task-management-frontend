<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
	
	public function detail($id = null)
	{
		if (!empty($id))
		{
			$params = ['id' => $id];
			$query  = http_build_query($params); 
			$url	= "http://localhost:8000/api/v1/task-detail";
			
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, $url.'?'.$query);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

			$headers = array();
			$headers[] = "Access-Control-Allow-Origin: *";
			$headers[] = "Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS";
			$headers[] = "Content-Type: application/json";
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			$result = curl_exec($ch);
			curl_close($ch);
			$data 	= json_decode($result, true);
			
			if ($data['success'])
			{
				return view('page.task.detail')
						->with('data', $data['data']);
			} else {
				abort(404);
			}
		} else {
            abort(404);
		}
	}
}