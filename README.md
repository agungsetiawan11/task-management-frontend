## About Laravel

Task management frontend is part of task management project to have function to show or display task list in front end

## How to Install

- git clone to local: git clone https://gitlab.com/agungsetiawan11/task-management-frontend.git
- go to directory
- composer install
- npm install
- npm run dev
- cp .env.example
- php artisan key:generate
- make sure backend part is already running on port 8000.
- php artisan serve --port=5000
- open Browser and pointing to URL http://localhost:5000/

## Creator

Agung Setiawan

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
